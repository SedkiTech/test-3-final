import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  sidebar: false,
};

export const sidebarSlice = createSlice({
  name: "sidebar",
  initialState,
  reducers: {
    showSidebar: (state, action) => {
      state.sidebar = !state.sidebar;
      console.log("state.sidebar:", state.sidebar);
    },
  },
});

export const sidebaractions = sidebarSlice.actions;

export default sidebarSlice.reducer;
