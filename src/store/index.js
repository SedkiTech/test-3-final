import { configureStore } from "@reduxjs/toolkit";
import filesReducer from "./slices/files";
import sidebarReducer from "./slices/sidebar";

export const store = configureStore({
  reducer: {
    files: filesReducer,
    sidebar: sidebarReducer,
  },
});
