import React from "react";

import Sidebar from "./Sidebar";
import Header from "./Header";

//import icons
import hamburgerMenu from "../../assets/icons/hamburger-menu.svg";

// material ui
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";

const style = {
  position: "absolute",
  top: "26%",
  left: "26%",
  transform: "translate(-50%, -50%)",
  width: 300,
  bgcolor: "background.paper",
  border: "2px solid #610bef",
  boxShadow: 24,
  p: 4,
};

const MainLayout = ({ children }) => {
  //material ui should
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div className="main-layout">
      <div className={`desktop-sidebar`}>
        <Sidebar />
      </div>

      {/* <button className="hamburger-nav">
        <img src={hamburgerMenu} alt="hamburgerMenu" />
      </button> */}

      <Button
        className="hamburger-nav"
        variant="outlined"
        size="small"
        onClick={handleOpen}
      >
        {" "}
        <img src={hamburgerMenu} alt="hamburgerMenu" />
      </Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Sidebar />
        </Box>
      </Modal>

      {/* <div className={`mobile-sidebar`}>
        <Humburger />
      </div> */}

      <div>
        <div className="header-container-main-layout">
          <Header />
        </div>
        {children}
      </div>
    </div>
  );
};

export default MainLayout;
