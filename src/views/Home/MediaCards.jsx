import React from "react";
//import styles
import "./_mediaCards.scss";
import Styled from "styled-components";
// import ICONS
import fileIcon from "../../assets/icons/File.svg";
import imageIcon from "../../assets/icons/Image.svg";
import videoIcon from "../../assets/icons/Video.svg";
// import utils
import bytesToSize from "../../utils/bytesToSize";
import volume from "../../constants/volume";
// import obj from "../../utils/calculateStorage";
import calculateStorage from "../../utils/calculateStorage";
import { useSelector } from "react-redux";

const Container = Styled.div`
      margin-left: 30px;
      margin-right: 30px;

  progress {
    margin-right: 8px;
  }

  progress[value] {
    width: ${(props) => props.width};

    -webkit-appearance: none;
    appearance: none;
  }

  progress[value]::-webkit-progress-bar {
    height: 12px;
    border-radius: 20px;
    background-color: #d9dbe9;
  }  

  progress[value]::-webkit-progress-value {
    height: 12px;
    border-radius: 20px;
    background-color: ${(props) => props.color};
  }
`;

const MediaCards = () => {
  const files = useSelector((state) => state.files.files);

  const obj = calculateStorage(files);
  const valueFile = obj.valueFile;

  console.log("valueFile", valueFile);

  const valueVideo = obj.valueVideo;
  console.log("valueVideo", valueVideo);

  const valueImage = obj.valueImage;
  console.log("valueImage", valueImage);

  let maxDoc = volume.docs;
  console.log("maxDoc", maxDoc);

  let maxImage = volume.imgs;
  console.log("maxImage", maxImage);

  let maxVideo = volume.videos;
  console.log("maxVideo", maxVideo);

  let width = "100%";
  const primary = "#610bef";
  const secondary = "#005bd4";
  const tertiary = "#008a00";

  return (
    <div className="media-container">
      {/* document box */}
      <div className="boxes box-1">
        <div className="icon-box icon-box-document-color">
          <img src={fileIcon} alt="fileIcon" />
        </div>
        <div className="documents_title">Documents</div>

        <Container color={primary} width={width}>
          <progress value={valueFile} max={maxDoc} />
          <div className="percentage-div">
            <div className="percentage-fixed">
              {" "}
              {((valueFile / maxDoc) * 100).toFixed(0)}%{" "}
            </div>
            {/* usage text  */}

            <div className="usage-div">
              {`${bytesToSize(valueFile)} of ${bytesToSize(maxDoc)} Used`}
            </div>
          </div>
        </Container>
      </div>
      {/* image box */}
      <div className="boxes ">
        <div className="icon-box icon-box-image-color ">
          <img src={imageIcon} alt="fileIcon" />
        </div>
        <div className="documents_title">Images</div>

        <Container color={secondary} width={width}>
          <progress value={valueImage} max={maxImage} />
          <div className="percentage-div">
            <div className="percentage-fixed">
              {" "}
              {((valueImage / maxImage) * 100).toFixed(0)}%{" "}
            </div>
            <div className="usage-div">
              {`${bytesToSize(valueImage)} of ${bytesToSize(maxImage)} Used`}
            </div>
          </div>
        </Container>

        {/* <div className="progress-bar">
          <div className="progress progress-image-color"></div>
        </div>
        <div className="progress-counter">
          <div className="percentage">29%</div>
          <div className="usage">300mb of 1gb Used</div>
        </div> */}
      </div>
      {/* video box */}
      <div className="boxes ">
        <div className="icon-box icon-box-video-color">
          <img src={videoIcon} alt="fileIcon" />
        </div>
        <div className="documents_title">Video</div>

        <Container color={tertiary} width={width}>
          <progress value={valueVideo} max={maxVideo} />
          <div className="percentage-div">
            <div className="percentage-fixed">
              {" "}
              {((valueVideo / maxVideo) * 100).toFixed(0)}%{" "}
            </div>
            <div className="usage-div">
              {`${bytesToSize(valueVideo)} of ${bytesToSize(maxVideo)} Used`}
            </div>
          </div>
        </Container>

        {/* <div className="progress-bar">
          <div className="progress progress-video-color"></div>
        </div>
        <div className="progress-counter">
          <div className="percentage">29%</div>
          <div className="usage">0.6gb of 5gb Used</div>
        </div> */}
      </div>
    </div>
  );
};

export default MediaCards;
