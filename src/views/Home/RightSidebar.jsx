import React from "react";
// import icons
import starIcon from "../../assets/icons/Star.svg";
import boxIcon from "../../assets/icons/Box.svg";

//router
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

const RightSidebar = () => {
  const files = useSelector((state) => state.files.files);

  const starredCounter = () => {
    let star = 0;
    files.map((item) => {
      if (item.isStarred) star++;
    });
    return star;
  };

  const archivedCounter = () => {
    let star = 0;

    files.map((item) => {
      if (item.isArchived) star++;
    });
    return star;
  };

  return (
    <div className="RightSidebar-container">
      {/* first card */}
      <div className="RightSidebar-starred-card-container">
        <div className="square-starred-files">
          <img src={starIcon} alt={"starIcon"} />
        </div>
        <div className="starred-files-text">
          <h4>{starredCounter()} Starred Files</h4>
          <h6>
            <Link to="/starred">Go to view</Link>
          </h6>
        </div>
      </div>

      {/* second card */}
      <div className="RightSidebar-box-card-container">
        <div className="square-box-files">
          <img src={boxIcon} alt={"boxIcon"} />
        </div>
        <div className="box-files-text">
          <h4>{archivedCounter()} Starred Files</h4>
          <h6>
            <Link to="/archived">Go to view</Link>
          </h6>
        </div>
      </div>
    </div>
  );
};

export default RightSidebar;
