import React from "react";
//import styles
import "./_HomeFilesTable.scss";

import { useSelector } from "react-redux";

//import ICONS

//import Utils
import bytesToSize from "../../utils/bytesToSize";
import dateToNewDate from "../../utils/dateToNewDate";
import guessFileType from "../../utils/guessFileType";
const HomeFilesTable = () => {
  const files = useSelector((state) => state.files.files);
  let list = files.slice(0, 5);

  return (
    <div className="homeFilesTable-container">
      <h1 className="title-Recent-files">Recent Files</h1>
      <table id="keywords" cellSpacing="0" cellPadding="0">
        <thead>
          <tr>
            <th>
              <span className="th-name">Name</span>
            </th>
            <th>
              <span className="th-date">Date created</span>
            </th>
            <th>
              <span className="th-size">Size</span>
            </th>
          </tr>
        </thead>
        <tbody>
          {list.map((file) => (
            <tr key={file.id}>
              <td>
                <img
                  src={guessFileType(file.type).type}
                  alt=""
                  className={guessFileType(file.type).style}
                />

                <span className="file-name">
                  {file.name}.{file.type}
                </span>
              </td>
              <td>{dateToNewDate(file.createdAt)}</td>

              <td> {bytesToSize(file.size)}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default HomeFilesTable;
