import StarredHeader from "./StarredHeader";
import StarredFilesTable from "./StarredFilesTable";

const Starred = () => {
  return (
    <div className="main-starred">
      <StarredHeader />
      <StarredFilesTable />
    </div>
  );
};

export default Starred;
