import React from "react";
//import styles
import "./_recentFiles.scss";

//import ICONS
import boxIcon from "../../assets/icons/Box.svg";
import starIcon from "../../assets/icons/Star.svg";
import fileStructureIcon from "../../assets/icons/FileStructure.svg";

//import Utils
import bytesToSize from "../../utils/bytesToSize";
import dateToNewDate from "../../utils/dateToNewDate";
import guessFileType from "../../utils/guessFileType";

//import components
import SortBy from "../../components/SortBy/SortBy";
//redux
import { useDispatch, useSelector } from "react-redux";
import { fileActions } from "../../store/slices/files";
const RecentFiles = () => {
  const dispatch = useDispatch();
  const files = useSelector((state) => state.files.files);

  return (
    <div className="recent-files-container ">
      <div className="all-files-container">
        <div className="left-container">
          <div className="square">
            <img src={fileStructureIcon} alt="fileStructureIcon" />
          </div>
          <h1 className="title">All Files</h1>
        </div>
        <div className="right-container">
          <SortBy />
        </div>
      </div>
      <table id="keywords" cellSpacing="0" cellPadding="0">
        <thead>
          <tr>
            <th>
              <span className="th-name">Name</span>
            </th>
            <th>
              <span className="th-date">Date created</span>
            </th>
            <th>
              <span className="th-size">Size</span>
            </th>
            <th>
              <span className="th-actions ">Actions</span>
            </th>
          </tr>
        </thead>
        <tbody>
          {files.map((file) => (
            <tr key={file.id}>
              <td>
                <div className="image-filename-wrapper">
                  <div className="parrent">
                    <img
                      src={guessFileType(file.type).type}
                      alt=""
                      className={guessFileType(file.type).style}
                    />

                    <div
                      className={`${
                        file.isArchived
                          ? "isArchived"
                          : file.isStarred
                          ? "isStarred"
                          : ""
                      }`}
                    ></div>
                  </div>

                  <span className="file-name">
                    {file.name}.{file.type}
                  </span>
                </div>
              </td>
              <td>{dateToNewDate(file.createdAt)}</td>

              <td> {bytesToSize(file.size)}</td>
              <td>
                <div className="elements-actions">
                  <button
                    className="elements-actions-star"
                    onClick={() => {
                      dispatch(fileActions.addStarredFile(file));
                    }}
                  >
                    <img src={starIcon} alt="starIcon" />
                  </button>
                  <button
                    className="elements-actions-box"
                    onClick={() => dispatch(fileActions.addArchivedFile(file))}
                  >
                    <img src={boxIcon} alt="boxIcon" />
                  </button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default RecentFiles;
