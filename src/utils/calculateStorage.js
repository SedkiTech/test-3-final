let obj = { valueFile: 0, valueVideo: 0, valueImage: 0 };

const calculateStorage = (files) => {
  let fileStorageUsed = 0;
  let videoStorageUsed = 0;
  let imageStorageUsed = 0;

  console.log("files.length", files.length);
  for (let i = 0; i < files.length; i++) {
    if (
      files[i].type === "png" ||
      files[i].type === "gif" ||
      files[i].type === "jpg"
    ) {
      imageStorageUsed += files[i].size;
      obj.valueImage = imageStorageUsed;
    }

    if (
      files[i].type === "avi" ||
      files[i].type === "mov" ||
      files[i].type === "mp4"
    ) {
      videoStorageUsed += files[i].size;
      obj.valueVideo = videoStorageUsed;
    }

    if (
      files[i].type === "txt" ||
      files[i].type === "pdf" ||
      files[i].type === "doc"
    ) {
      fileStorageUsed += files[i].size;
      obj.valueFile = fileStorageUsed;
    }
  }

  return obj;
};

export default calculateStorage;
