const dateToNewDate = (date) => {
  const newDate = new Date(date);
  const month = newDate.toLocaleString("en-US", { month: "long" });
  let day = newDate.toLocaleString("en-US", { day: "2-digit" });
  if (day < 10) {
    day = day.substr(1, 2);
  }
  const year = newDate.getFullYear();

  return `${month} ${day}, ${year}`;
};

export default dateToNewDate;
